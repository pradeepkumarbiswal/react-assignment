import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Movie from ".";

describe("Movie", () => {

  let wrapper: any
  beforeAll(() => {
    wrapper = shallow(<Movie />);
  });

  it("matches snapshot", () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it('render component', () => {
    expect(wrapper.find('div').length).toEqual(1)
  });

  it('render component', () => {
    expect(wrapper.find('MovieDetails').length).toEqual(1)
  });

});